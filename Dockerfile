FROM node:lts-alpine
WORKDIR /usr/src/app
RUN apk update && apk upgrade
COPY package.json .
RUN yarn install
COPY . .
RUN yarn run build
EXPOSE 3000
CMD ["yarn", "run", "start:prod"]
