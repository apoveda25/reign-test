## Previous requirements

```bash
$ 'dev.reign.test' >> /etc/hosts

$ cp .development.env .env
```

## Running the app

```bash
# production
$ docker-compose up -d

# development
$ docker-compose -f docker-compose.dev.yml up -d
```

Navigate to [reign local](http://dev.reign.test)
