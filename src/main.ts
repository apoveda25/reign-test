import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );

  const logger = new Logger('Application');
  const configService = app.get(ConfigService);

  app.setGlobalPrefix('api/v1');

  const config = new DocumentBuilder()
    .setTitle('Reign Test')
    .setDescription('The technical test API')
    .setVersion('1.0')
    .addBearerAuth()
    .addTag('Articles')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api/docs', app, document);

  await app.listen(
    configService.get('APP_PORT'),
    configService.get('APP_HOST'),
  );

  logger.verbose(`Server listening on ${await app.getUrl()} 🚀`);
}
bootstrap();
