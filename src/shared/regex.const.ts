export const tagsRegex = /^(\w)+(,[\w]+)*$/;
export const usernameRegex = /^[\w]{3,}$/;
export const passwordRegex = /^[\w.,:!@#$%&*]{8,}$/;
