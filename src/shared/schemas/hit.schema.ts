import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import mongoose, { Document } from 'mongoose';
import {
  IHighlight,
  IHighlightResult,
  IHit,
} from '../interfaces/payload.interface';

export type HitDocument = Hit & Document;

@Schema()
export class Highlight implements IHighlight {
  @ApiProperty()
  @Prop()
  value: string;

  @ApiProperty()
  @Prop()
  matchLevel: string;

  @ApiProperty()
  @Prop({ type: [String] })
  matchedWords: string[];

  @ApiProperty()
  @Prop()
  fullyHighlighted: boolean;
}

export const HighlightSchema = SchemaFactory.createForClass(Highlight);

@Schema()
export class HighlightResult implements IHighlightResult {
  @ApiProperty({ type: Highlight })
  @Prop({
    type: Map,
    of: HighlightSchema,
    default: {},
  })
  title: IHighlight;

  @ApiProperty({ type: Highlight })
  @Prop({
    type: Map,
    of: HighlightSchema,
    default: {},
  })
  author: IHighlight;

  @ApiProperty({ type: Highlight })
  @Prop({
    type: Map,
    of: HighlightSchema,
    default: {},
  })
  story_text: IHighlight;

  @ApiProperty({ type: Highlight })
  @Prop({
    type: Map,
    of: HighlightSchema,
    default: {},
  })
  url: IHighlight;
}

export const HighlightResultSchema =
  SchemaFactory.createForClass(HighlightResult);

@Schema()
export class Hit implements IHit {
  @ApiProperty({ type: String })
  @Prop({ type: mongoose.Types.ObjectId })
  _id: mongoose.Types.ObjectId;

  @ApiProperty()
  @Prop({ default: false })
  deleted: boolean;

  @ApiProperty()
  @Prop()
  created_at: Date;

  @ApiProperty()
  @Prop()
  title: string;

  @ApiProperty()
  @Prop()
  url: string;

  @ApiProperty()
  @Prop()
  author: string;

  @ApiProperty()
  @Prop()
  points: number;

  @ApiProperty()
  @Prop()
  story_text: string;

  @ApiProperty()
  @Prop({ required: false })
  comment_text: string;

  @ApiProperty()
  @Prop()
  num_comments: number;

  @ApiProperty()
  @Prop({ required: false, type: String })
  story_id: string | null;

  @ApiProperty()
  @Prop({ required: false, type: String })
  story_title: string | null;

  @ApiProperty()
  @Prop({ required: false, type: String })
  story_url: string | null;

  @ApiProperty()
  @Prop({ required: false, type: String })
  parent_id: string | null;

  @ApiProperty()
  @Prop()
  created_at_i: number;

  @ApiProperty()
  @Prop({ type: [String] })
  _tags: string[];

  @ApiProperty()
  @Prop({ unique: true })
  objectID: string;

  @ApiProperty({ type: HighlightResult })
  @Prop({
    type: Map,
    of: HighlightResultSchema,
    default: {},
  })
  _highlightResult: IHighlightResult;
}

export const HitSchema = SchemaFactory.createForClass(Hit);
