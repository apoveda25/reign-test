export interface IHighlight {
  value: string;
  matchLevel: string;
  fullyHighlighted: boolean;
  matchedWords: string[];
}

export interface IHighlightResult {
  title: IHighlight;
  author: IHighlight;
  story_text: IHighlight;
  url: IHighlight;
}

export interface IHit {
  created_at: Date;
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string | null;
  num_comments: number;
  story_id: string | null;
  story_title: string | null;
  story_url: string | null;
  parent_id: string | null;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: IHighlightResult;
}

export interface IPayload {
  hits: IHit[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  exhaustiveTypo: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}
