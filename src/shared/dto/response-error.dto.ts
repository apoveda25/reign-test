import { ApiProperty } from '@nestjs/swagger';
import { IResponse } from '../interfaces/response.interface';

export class ResponseError implements IResponse {
  @ApiProperty()
  message: string;

  @ApiProperty()
  statusCode: number;
}
