import { plainToClass } from 'class-transformer';
import {
  IsHexadecimal,
  IsIP,
  IsPort,
  IsString,
  IsUrl,
  validateSync,
} from 'class-validator';

class EnvironmentVariables {
  @IsPort()
  APP_PORT: number;

  @IsIP()
  APP_HOST: string;

  @IsUrl()
  MONGO_URI: string;

  @IsUrl()
  API_BASE_URL: string;

  @IsString()
  SCHEDULE_SAVE_ALL: string;

  @IsHexadecimal()
  JWT_SECRET: string;

  @IsString()
  JWT_EXPIRES_IN: string;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });

  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }

  return validatedConfig;
}
