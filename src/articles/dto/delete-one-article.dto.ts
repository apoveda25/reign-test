import { IsMongoId, IsString } from 'class-validator';

export class DeleteOneArticleDto {
  @IsMongoId()
  @IsString()
  id: string;
}
