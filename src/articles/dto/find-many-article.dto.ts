import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsEnum,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';

export enum Months {
  JANUARY = 'january',
  FEBRUARY = 'february',
  MARCH = 'march',
  APRIL = 'april',
  MAY = 'may',
  JUNE = 'june',
  JULY = 'july',
  AUGUST = 'august',
  SEPTEMBER = 'september',
  OCTOBER = 'october',
  NOVEMBER = 'november',
  DECEMBER = 'december',
}

export class FindManyDto {
  @IsString()
  @IsOptional()
  author?: string;

  @IsArray()
  @IsOptional()
  _tags?: string[];

  @IsString()
  @IsOptional()
  title?: string;

  @IsEnum(Months)
  @IsOptional()
  created_at?: Months;

  @Min(0)
  page: number;

  @Max(5)
  @Min(1)
  docsPerPage: number;
}

export class FindManyInput {
  @ApiProperty({ description: 'Name of author', required: false })
  author?: string;

  @ApiProperty({
    description: 'Tag names separated by commas',
    required: false,
  })
  _tags?: string;

  @ApiProperty({ description: 'Title of article', required: false })
  title?: string;

  @ApiProperty({
    description: 'Creation month name',
    required: false,
    enum: Months,
  })
  created_at?: Months;

  @ApiProperty({ description: 'Number of page', required: false, minimum: 0 })
  page?: number;

  @ApiProperty({
    description: 'Number of documents by page',
    required: false,
    minimum: 1,
  })
  docsPerPage?: number;
}
