import faker from '@faker-js/faker';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { FilterQuery, Model } from 'mongoose';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';
import { DeleteOneArticleDto } from '../dto/delete-one-article.dto';
import { ArticlesRepository } from './articles.repository';

describe('ArticlesRepository', () => {
  let provider: ArticlesRepository;
  let hitModel: Model<HitDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesRepository,
        {
          provide: getModelToken(Hit.name),
          useValue: {
            find: jest.fn(() => ({
              skip: jest.fn(() => ({
                limit: jest.fn(() => ({ exec: jest.fn() })),
              })),
            })),
            findOneAndUpdate: jest.fn(() => ({ exec: jest.fn() })),
          },
        },
      ],
    }).compile();

    provider = module.get<ArticlesRepository>(ArticlesRepository);
    hitModel = module.get<Model<HitDocument>>(getModelToken(Hit.name));
  });

  test('should be defined', () => {
    expect(provider).toBeDefined();
    expect(hitModel).toBeDefined();
  });

  describe('findMany', () => {
    test('should return one array of articles', async () => {
      /**
       * Arrange
       */
      const query = {
        _id: faker.datatype.uuid(),
        objectID: faker.datatype.uuid(),
      };
      const args: FilterQuery<HitDocument> = {
        ...query,
        limit: faker.datatype.number({ min: 1, max: 5 }),
        skip: faker.datatype.number({ min: 0, max: 100 }),
      };

      const hitModelFindSpy = jest.spyOn(hitModel, 'find');

      /**
       * Act
       */
      await provider.findMany(args);

      /**
       * Assert
       */
      expect(hitModelFindSpy).toHaveBeenCalledWith(query);
    });
  });

  describe('deleteOne', () => {
    test('should return one article deleted', async () => {
      /**
       * Arrange
       */
      const deleteOneArticleDto: DeleteOneArticleDto = {
        id: faker.datatype.uuid(),
      };
      const filter = { _id: deleteOneArticleDto.id, deleted: false };
      const update = { deleted: true };

      const hitModelFindOneAndUpdateSpy = jest.spyOn(
        hitModel,
        'findOneAndUpdate',
      );

      /**
       * Act
       */
      await provider.deleteOne(deleteOneArticleDto);

      /**
       * Assert
       */
      expect(hitModelFindOneAndUpdateSpy).toHaveBeenCalledWith(filter, update);
    });
  });
});
