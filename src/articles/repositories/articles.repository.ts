import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';
import { DeleteOneArticleDto } from '../dto/delete-one-article.dto';

@Injectable()
export class ArticlesRepository {
  constructor(@InjectModel(Hit.name) private hitModel: Model<HitDocument>) {}

  async findMany({
    limit,
    skip,
    ...params
  }: FilterQuery<HitDocument>): Promise<HitDocument[]> {
    return this.hitModel.find(params).skip(skip).limit(limit).exec();
  }

  async deleteOne(params: DeleteOneArticleDto): Promise<HitDocument> {
    return this.hitModel
      .findOneAndUpdate({ _id: params.id, deleted: false }, { deleted: true })
      .exec();
  }
}
