import { Injectable } from '@nestjs/common';
import { Months } from '../dto/find-many-article.dto';

@Injectable()
export class MonthsService {
  private readonly months = {
    january: { numberMonth: 0, lastMonthDay: 31 },
    february: { numberMonth: 1, lastMonthDay: 28 },
    march: { numberMonth: 2, lastMonthDay: 31 },
    april: { numberMonth: 3, lastMonthDay: 30 },
    may: { numberMonth: 4, lastMonthDay: 31 },
    june: { numberMonth: 5, lastMonthDay: 30 },
    july: { numberMonth: 6, lastMonthDay: 31 },
    august: { numberMonth: 7, lastMonthDay: 31 },
    september: { numberMonth: 8, lastMonthDay: 30 },
    october: { numberMonth: 9, lastMonthDay: 31 },
    november: { numberMonth: 10, lastMonthDay: 30 },
    december: { numberMonth: 11, lastMonthDay: 31 },
  };

  getDateFromMonthMin(month: Months): Date {
    const monthMin = new Date();

    monthMin.setMonth(this.months[month].numberMonth, 1);
    monthMin.setHours(0, 0, 0, 0);

    return monthMin;
  }

  getDateFromMonthMax(month: Months): Date {
    const monthMax = new Date();

    monthMax.setMonth(
      this.months[month].numberMonth,
      this.isLeapYear(monthMax) &&
        this.isFebruary(this.months[month].numberMonth)
        ? this.months[month].lastMonthDay + 1
        : this.months[month].lastMonthDay,
    );
    monthMax.setHours(23, 59, 59, 999);

    return monthMax;
  }

  private isLeapYear(date: Date): boolean {
    const year = date.getFullYear();

    return (
      this.isDivisible(year, 4) &&
      (!this.isDivisible(year, 100) || this.isDivisible(year, 400))
    );
  }

  private isFebruary(numberMonth: number): boolean {
    return numberMonth === 1;
  }

  private isDivisible(dividend: number, divider: number): boolean {
    return dividend / divider === 0;
  }
}
