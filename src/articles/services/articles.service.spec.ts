import faker from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import mongoose from 'mongoose';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';
import { DeleteOneArticleDto } from '../dto/delete-one-article.dto';
import { FindManyDto, Months } from '../dto/find-many-article.dto';
import { ArticlesRepository } from '../repositories/articles.repository';
import { ArticlesService } from './articles.service';
import { MonthsService } from './months.service';

describe('ArticlesService', () => {
  let service: ArticlesService;
  let articlesRepository: ArticlesRepository;
  let monthsService: MonthsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        {
          provide: ArticlesRepository,
          useFactory: () => ({ findMany: jest.fn(), deleteOne: jest.fn() }),
        },
        {
          provide: MonthsService,
          useFactory: () => ({
            getDateFromMonthMin: jest.fn(),
            getDateFromMonthMax: jest.fn(),
          }),
        },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
    articlesRepository = module.get<ArticlesRepository>(ArticlesRepository);
    monthsService = module.get<MonthsService>(MonthsService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
    expect(articlesRepository).toBeDefined();
    expect(monthsService).toBeDefined();
  });

  describe('findAll', () => {
    test('should return one array of articles', async () => {
      /**
       * Arrange
       */
      const findManyDto: FindManyDto = {
        author: faker.name.firstName(),
        _tags: [faker.company.companyName()],
        title: faker.name.title(),
        created_at: Months.JANUARY,
        page: faker.datatype.number({ min: 0, max: 10 }),
        docsPerPage: faker.datatype.number({ min: 1, max: 5 }),
      };
      const article: Hit = {
        _id: new mongoose.Types.ObjectId(),
        deleted: false,
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };

      const articlesRepositoryFindManySpy = jest
        .spyOn(articlesRepository, 'findMany')
        .mockImplementation()
        .mockReturnValue(
          new Promise((resolve) => resolve([article] as HitDocument[])),
        );
      const monthsServiceGetDateFromMonthMinSpy = jest
        .spyOn(monthsService, 'getDateFromMonthMin')
        .mockReturnValue(new Date());

      const monthsServiceGetDateFromMonthMaxSpy = jest
        .spyOn(monthsService, 'getDateFromMonthMax')
        .mockReturnValue(new Date());

      /**
       * Act
       */
      const result = await service.findAll(findManyDto);

      /**
       * Assert
       */
      expect(articlesRepositoryFindManySpy).toHaveBeenCalled();
      expect(monthsServiceGetDateFromMonthMinSpy).toHaveBeenCalled();
      expect(monthsServiceGetDateFromMonthMaxSpy).toHaveBeenCalled();
      expect(result).toEqual([article]);
    });
  });

  describe('remove', () => {
    test('should return one article deleted', async () => {
      /**
       * Arrange
       */
      const deleteOneArticleDto: DeleteOneArticleDto = {
        id: faker.datatype.uuid(),
      };
      const article: Hit = {
        _id: new mongoose.Types.ObjectId(),
        deleted: false,
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };

      const articlesRepositoryDeleteOneSpy = jest
        .spyOn(articlesRepository, 'deleteOne')
        .mockImplementation()
        .mockReturnValue(
          new Promise((resolve) => resolve(article as HitDocument)),
        );

      /**
       * Act
       */
      const result = await service.remove(deleteOneArticleDto);

      /**
       * Assert
       */
      expect(articlesRepositoryDeleteOneSpy).toHaveBeenCalledWith(
        deleteOneArticleDto,
      );
      expect(result).toEqual(article);
    });
  });
});
