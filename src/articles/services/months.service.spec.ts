import { Test, TestingModule } from '@nestjs/testing';
import { Months } from '../dto/find-many-article.dto';
import { MonthsService } from './months.service';

describe('MonthsService', () => {
  let service: MonthsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MonthsService],
    }).compile();

    service = module.get<MonthsService>(MonthsService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getDateFromMonthMin', () => {
    test('should return one date', async () => {
      /**
       * Arrange
       */
      const month = Months.JANUARY;
      const numberMonth = 0;

      /**
       * Act
       */
      const result = service.getDateFromMonthMin(month);

      /**
       * Assert
       */
      expect(result.getMonth()).toEqual(numberMonth);
    });
  });

  describe('getDateFromMonthMax', () => {
    test('should return one date', async () => {
      /**
       * Arrange
       */
      const month = Months.JANUARY;
      const numberMonth = 0;

      /**
       * Act
       */
      const result = service.getDateFromMonthMax(month);

      /**
       * Assert
       */
      expect(result.getMonth()).toEqual(numberMonth);
    });
  });
});
