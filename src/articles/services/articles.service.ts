import { Injectable, NotFoundException } from '@nestjs/common';
import { HitDocument } from '../../shared/schemas/hit.schema';
import { DeleteOneArticleDto } from '../dto/delete-one-article.dto';
import { FindManyDto } from '../dto/find-many-article.dto';
import { ArticlesRepository } from '../repositories/articles.repository';
import { MonthsService } from './months.service';

@Injectable()
export class ArticlesService {
  constructor(
    private articlesRepository: ArticlesRepository,
    private monthsService: MonthsService,
  ) {}

  async findAll({
    page,
    docsPerPage,
    created_at,
    ...args
  }: FindManyDto): Promise<HitDocument[]> {
    const tags = args._tags ? { _tags: { $in: args._tags } } : {};
    const createdAt = created_at
      ? {
          $and: [
            {
              created_at: {
                $gte: this.monthsService.getDateFromMonthMin(created_at),
              },
            },
            {
              created_at: {
                $lte: this.monthsService.getDateFromMonthMax(created_at),
              },
            },
          ],
        }
      : {};
    const query = {
      skip: page * docsPerPage,
      limit: docsPerPage,
      deleted: false,
      ...createdAt,
      ...args,
      ...tags,
    };

    return this.articlesRepository.findMany(query);
  }

  async remove(params: DeleteOneArticleDto) {
    const deleted = await this.articlesRepository.deleteOne(params);

    if (!deleted) throw new NotFoundException();

    return deleted;
  }
}
