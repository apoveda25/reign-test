import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Hit, HitSchema } from '../shared/schemas/hit.schema';
import { ArticlesController } from './controllers/articles.controller';
import { ArticlesRepository } from './repositories/articles.repository';
import { ArticlesService } from './services/articles.service';
import { MonthsService } from './services/months.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }])],
  controllers: [ArticlesController],
  providers: [ArticlesService, ArticlesRepository, MonthsService],
})
export class ArticlesModule {}
