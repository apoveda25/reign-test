import faker from '@faker-js/faker';
import {
  FindManyDto,
  FindManyInput,
  Months,
} from '../dto/find-many-article.dto';
import { FindManyPipe } from './find-many.pipe';

describe('FindManyPipe', () => {
  const findManyPipe = new FindManyPipe();

  test('should be defined', () => {
    expect(findManyPipe).toBeDefined();
  });

  describe('transform', () => {
    test('should return one dto', async () => {
      /**
       * Arrange
       */
      const findManyInput: FindManyInput = {
        author: faker.name.firstName(),
        _tags: faker.name.jobArea(),
        title: faker.name.title(),
        created_at: Months.FEBRUARY,
        page: faker.datatype.number({ min: 0, max: 10 }),
        docsPerPage: faker.datatype.number({ min: 1, max: 5 }),
      };
      const findManyDto: FindManyDto = {
        ...findManyInput,
        _tags: [findManyInput._tags],
        page: findManyInput.page,
        docsPerPage: findManyInput.docsPerPage,
      };

      /**
       * Act
       */
      const result = findManyPipe.transform(findManyInput);

      /**
       * Assert
       */
      expect(result.author).toMatch(findManyDto.author);
      expect(result._tags).toContain(findManyDto._tags[0]);
      expect(result.title).toMatch(findManyDto.title);
      expect(result.created_at).toMatch(findManyDto.created_at);
      expect(result.page).toBeGreaterThanOrEqual(0);
      expect(result.docsPerPage).toBeGreaterThanOrEqual(1);
      expect(result.docsPerPage).toBeLessThanOrEqual(5);
    });
  });
});
