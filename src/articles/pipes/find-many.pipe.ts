import { ConflictException, Injectable, PipeTransform } from '@nestjs/common';
import { tagsRegex } from '../../shared/regex.const';
import { FindManyDto, FindManyInput } from '../dto/find-many-article.dto';

@Injectable()
export class FindManyPipe implements PipeTransform {
  transform({
    author,
    _tags,
    title,
    created_at,
    page,
    docsPerPage,
  }: FindManyInput): FindManyDto {
    if (_tags && !tagsRegex.test(_tags)) throw new ConflictException();

    const dto = {
      author,
      _tags: _tags ? _tags.split(',') : null,
      title,
      created_at,
      page: page ? +page : null,
      docsPerPage: docsPerPage ? +docsPerPage : null,
    };

    return Object.entries(dto).reduce(
      (prev, [key, value]) =>
        value == null ? { ...prev } : { ...prev, [key]: value },
      { page: 0, docsPerPage: 5 },
    );
  }
}
