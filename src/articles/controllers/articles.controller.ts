import {
  Controller,
  Delete,
  Get,
  Param,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { ResponseError } from '../../shared/dto/response-error.dto';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';
import { DeleteOneArticleDto } from '../dto/delete-one-article.dto';
import { FindManyDto, FindManyInput } from '../dto/find-many-article.dto';
import { FindManyPipe } from '../pipes/find-many.pipe';
import { ArticlesService } from '../services/articles.service';

@ApiTags('Articles')
@ApiBearerAuth()
@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Get()
  @ApiQuery({ type: FindManyInput })
  @ApiOkResponse({ type: [Hit] })
  @ApiUnauthorizedResponse({ type: ResponseError })
  @ApiInternalServerErrorResponse({ type: ResponseError })
  @UseGuards(JwtAuthGuard)
  @UsePipes(FindManyPipe, new ValidationPipe({ expectedType: FindManyDto }))
  async findAll(@Query() query: FindManyDto): Promise<HitDocument[]> {
    return this.articlesService.findAll(query);
  }

  @Delete(':id')
  @ApiParam({ name: 'id ', type: String })
  @ApiOkResponse({ type: Hit })
  @ApiNotFoundResponse({ type: ResponseError })
  @ApiUnauthorizedResponse({ type: ResponseError })
  @ApiInternalServerErrorResponse({ type: ResponseError })
  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe({ expectedType: DeleteOneArticleDto }))
  async remove(@Param() params: DeleteOneArticleDto) {
    return this.articlesService.remove(params);
  }
}
