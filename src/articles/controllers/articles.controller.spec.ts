import { faker } from '@faker-js/faker';
import { ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import mongoose from 'mongoose';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';
import { DeleteOneArticleDto } from '../dto/delete-one-article.dto';
import { FindManyDto, Months } from '../dto/find-many-article.dto';
import { FindManyPipe } from '../pipes/find-many.pipe';
import { ArticlesService } from '../services/articles.service';
import { ArticlesController } from './articles.controller';

describe('ArticlesController', () => {
  let controller: ArticlesController;
  let articlesService: ArticlesService;
  const mockValidationPipe = jest.fn();
  const mockFindManyPipe = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [
        {
          provide: ArticlesService,
          useFactory: () => ({ findAll: jest.fn(), remove: jest.fn() }),
        },
      ],
    })
      .overridePipe(FindManyPipe)
      .useValue(mockFindManyPipe)
      .overridePipe(ValidationPipe)
      .useValue(mockValidationPipe)
      .compile();

    controller = module.get<ArticlesController>(ArticlesController);
    articlesService = module.get<ArticlesService>(ArticlesService);
  });

  test('should be defined', () => {
    expect(controller).toBeDefined();
    expect(articlesService).toBeDefined();
  });

  describe('findAll', () => {
    test('should return one array of articles', async () => {
      /**
       * Arrange
       */
      const findManyDto: FindManyDto = {
        author: faker.name.firstName(),
        _tags: [faker.name.jobType()],
        title: faker.name.title(),
        created_at: Months.FEBRUARY,
        page: faker.datatype.number({ min: 0, max: 10 }),
        docsPerPage: faker.datatype.number({ min: 1, max: 5 }),
      };
      const article: Hit = {
        _id: new mongoose.Types.ObjectId(),
        deleted: false,
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };

      const articlesServiceFindAllSpy = jest
        .spyOn(articlesService, 'findAll')
        .mockResolvedValue([article as Hit & HitDocument]);

      /**
       * Act
       */
      const result = await controller.findAll(findManyDto);

      /**
       * Assert
       */
      expect(articlesServiceFindAllSpy).toHaveBeenCalledWith(findManyDto);
      expect(result).toEqual([article]);
    });
  });

  describe('remove', () => {
    test('should return one article deleted', async () => {
      /**
       * Arrange
       */
      const deleteOneArticleDto: DeleteOneArticleDto = {
        id: faker.datatype.uuid(),
      };
      const article: Hit = {
        _id: new mongoose.Types.ObjectId(),
        deleted: false,
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };

      const articlesServiceRemoveSpy = jest
        .spyOn(articlesService, 'remove')
        .mockResolvedValue(article as Hit & HitDocument);

      /**
       * Act
       */
      const result = await controller.remove(deleteOneArticleDto);

      /**
       * Assert
       */
      expect(articlesServiceRemoveSpy).toHaveBeenCalledWith(
        deleteOneArticleDto,
      );
      expect(result).toEqual(article);
    });
  });
});
