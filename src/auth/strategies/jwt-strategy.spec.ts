import faker from '@faker-js/faker';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { IUser } from '../interfaces/request-login.interface';
import { JwtStrategy } from './jwt-strategy';

describe('JwtStrategy', () => {
  let provider: JwtStrategy;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        {
          provide: ConfigService,
          useFactory: () => ({
            get: jest.fn(() => faker.internet.password(48)),
          }),
        },
      ],
    }).compile();

    provider = module.get<JwtStrategy>(JwtStrategy);
    configService = module.get<ConfigService>(ConfigService);
  });

  test('should be defined', () => {
    expect(provider).toBeDefined();
    expect(configService).toBeDefined();
  });

  describe('findAll', () => {
    test('should return one array of articles', async () => {
      /**
       * Arrange
       */
      const payload: {
        sub: string;
        username: string;
      } = {
        sub: faker.datatype.uuid(),
        username: faker.internet.userName(),
      };
      const user: Omit<IUser, 'password'> = {
        _id: payload.sub,
        username: payload.username,
      };

      /**
       * Act
       */
      const result = await provider.validate(payload);

      /**
       * Assert
       */
      expect(result).toEqual(user);
    });
  });
});
