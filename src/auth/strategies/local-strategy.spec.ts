import faker from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { IUser } from '../interfaces/request-login.interface';
import { AuthService } from '../services/auth.service';
import { LocalStrategy } from './local-strategy';

describe('LocalStrategy', () => {
  let provider: LocalStrategy;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LocalStrategy,
        {
          provide: AuthService,
          useFactory: () => ({ validateUser: jest.fn() }),
        },
      ],
    }).compile();

    provider = module.get<LocalStrategy>(LocalStrategy);
    authService = module.get<AuthService>(AuthService);
  });

  test('should be defined', () => {
    expect(provider).toBeDefined();
    expect(authService).toBeDefined();
  });

  describe('validate', () => {
    test('should return one user', async () => {
      /**
       * Arrange
       */
      const username = faker.internet.userName();
      const password = faker.internet.password();
      const user: Omit<IUser, 'password'> = {
        _id: faker.datatype.uuid(),
        username,
      };

      const authServiceValidateUserSpy = jest
        .spyOn(authService, 'validateUser')
        .mockResolvedValue(user);

      /**
       * Act
       */
      const result = await provider.validate(username, password);

      /**
       * Assert
       */
      expect(authServiceValidateUserSpy).toHaveBeenCalledWith({
        username,
        password,
      });
      expect(result).toEqual(user);
    });
  });
});
