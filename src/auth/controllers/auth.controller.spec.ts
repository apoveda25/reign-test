import faker from '@faker-js/faker';
import { Test, TestingModule } from '@nestjs/testing';
import { IRequestLogin } from '../interfaces/request-login.interface';
import { AuthService } from '../services/auth.service';
import { AuthController } from './auth.controller';

describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useFactory: () => ({ login: jest.fn() }),
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  test('should be defined', () => {
    expect(controller).toBeDefined();
    expect(authService).toBeDefined();
  });

  describe('login', () => {
    test('should return one access token', async () => {
      /**
       * Arrange
       */
      const userDto = {
        user: {
          username: faker.internet.userName(),
          _id: faker.datatype.uuid(),
        },
      } as IRequestLogin;
      const accessToken = { access_token: '' };

      const authServiceLoginSpy = jest
        .spyOn(authService, 'login')
        .mockResolvedValue(accessToken);

      /**
       * Act
       */
      const result = await controller.login(userDto);

      /**
       * Assert
       */
      expect(authServiceLoginSpy).toHaveBeenCalledWith(userDto.user);
      expect(result).toEqual(accessToken);
    });
  });
});
