import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import {
  ApiBody,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ResponseError } from '../../shared/dto/response-error.dto';
import { LoginDto } from '../dto/login.dto';
import { UserDto } from '../dto/user-response.dto';
import { LocalAuthGuard } from '../guards/local-auth.guard';
import { IRequestLogin } from '../interfaces/request-login.interface';
import { AuthService } from '../services/auth.service';

@ApiTags('Authetication')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  @ApiBody({ type: LoginDto })
  @ApiOkResponse({ type: UserDto })
  @ApiUnauthorizedResponse({ type: ResponseError })
  @ApiInternalServerErrorResponse({ type: ResponseError })
  @UseGuards(LocalAuthGuard)
  async login(@Request() request: IRequestLogin) {
    return this.authService.login(request.user);
  }
}
