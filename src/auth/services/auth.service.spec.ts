import faker from '@faker-js/faker';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { IUser } from '../interfaces/request-login.interface';
import { UsersRepository } from '../repositories/users-repository';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let usersRepository: UsersRepository;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersRepository,
          useFactory: () => ({ findOne: jest.fn() }),
        },
        { provide: JwtService, useFactory: () => ({ sign: jest.fn() }) },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    usersRepository = module.get<UsersRepository>(UsersRepository);
    jwtService = module.get<JwtService>(JwtService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
    expect(usersRepository).toBeDefined();
    expect(jwtService).toBeDefined();
  });

  describe('validateUser', () => {
    test('should return one user', async () => {
      /**
       * Arrange
       */
      const userDto: Omit<IUser, '_id'> = {
        username: faker.internet.userName(),
        password: faker.internet.password(),
      };
      const user: Omit<IUser, 'password'> = {
        _id: faker.datatype.uuid(),
        username: userDto.username,
      };
      const userDB: IUser = {
        ...userDto,
        ...user,
      };

      const usersRepositoryFindOneSpy = jest
        .spyOn(usersRepository, 'findOne')
        .mockResolvedValue(userDB);

      /**
       * Act
       */
      const result = await service.validateUser(userDto);

      /**
       * Assert
       */
      expect(usersRepositoryFindOneSpy).toHaveBeenCalledWith({
        username: userDto.username,
      });
      expect(result).toEqual(user);
    });
  });

  describe('login', () => {
    test('should return one token', async () => {
      /**
       * Arrange
       */

      const userDto: Omit<IUser, 'password'> = {
        _id: faker.datatype.uuid(),
        username: faker.internet.userName(),
      };
      const payload = { username: userDto.username, sub: userDto._id };
      const token = {
        access_token: '',
      };

      const jwtServiceFindOneSpy = jest
        .spyOn(jwtService, 'sign')
        .mockReturnValue('');

      /**
       * Act
       */
      const result = await service.login(userDto);

      /**
       * Assert
       */
      expect(jwtServiceFindOneSpy).toHaveBeenCalledWith(payload);
      expect(result).toEqual(token);
    });
  });
});
