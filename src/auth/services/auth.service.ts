import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUser } from '../interfaces/request-login.interface';
import { UsersRepository } from '../repositories/users-repository';

@Injectable()
export class AuthService {
  constructor(
    private usersRepository: UsersRepository,
    private jwtService: JwtService,
  ) {}

  async validateUser(
    args: Omit<IUser, '_id'>,
  ): Promise<Omit<IUser, 'password'>> {
    const result = await this.usersRepository.findOne({
      username: args.username,
    });

    if (!result) return null;

    const { password, ...user } = result;

    if (password === args.password) return user;

    return null;
  }

  async login(
    user: Omit<IUser, 'password'>,
  ): Promise<{ access_token: string }> {
    const payload = { username: user.username, sub: user._id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
