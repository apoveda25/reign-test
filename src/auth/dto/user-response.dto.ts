import { ApiProperty } from '@nestjs/swagger';
import { IUser } from '../../auth/interfaces/request-login.interface';

export class UserDto implements Omit<IUser, 'password'> {
  @ApiProperty()
  _id: string;

  @ApiProperty()
  username: string;
}
