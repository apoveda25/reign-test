import { ApiProperty } from '@nestjs/swagger';
import { IUser } from '../interfaces/request-login.interface';

export class LoginDto implements Omit<IUser, '_id'> {
  @ApiProperty({ default: 'usernameTest' })
  username: string;

  @ApiProperty({ default: 'secret123' })
  password: string;
}
