import { Test, TestingModule } from '@nestjs/testing';
import { IUser } from '../interfaces/request-login.interface';
import { UsersRepository } from './users-repository';

describe('UsersRepository', () => {
  let provider: UsersRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersRepository],
    }).compile();

    provider = module.get<UsersRepository>(UsersRepository);
  });

  test('should be defined', () => {
    expect(provider).toBeDefined();
  });

  describe('remove', () => {
    test('should return one article deleted', async () => {
      /**
       * Arrange
       */
      const userDto: Partial<Omit<IUser, 'password'>> = {
        _id: '',
        username: 'usernameTest',
      };
      const user: Partial<IUser> = {
        ...userDto,
        password: 'secret123',
      };

      /**
       * Act
       */
      const result = await provider.findOne(userDto);

      /**
       * Assert
       */
      expect(result).toEqual(user);
    });
  });
});
