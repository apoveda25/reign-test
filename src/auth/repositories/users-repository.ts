import { Injectable } from '@nestjs/common';
import { IUser } from '../interfaces/request-login.interface';

@Injectable()
export class UsersRepository {
  private readonly users: IUser[] = [
    {
      _id: '',
      username: 'usernameTest',
      password: 'secret123',
    },
  ];

  async findOne(args: Partial<Omit<IUser, 'password'>>): Promise<IUser> {
    return this.users.filter((user) => user.username === args.username).shift();
  }
}
