import { FastifyRequest } from 'fastify';

export interface IUser {
  _id: string;
  username: string;
  password: string;
}

export interface IRequestLogin extends FastifyRequest {
  user: Omit<IUser, 'password'>;
}
