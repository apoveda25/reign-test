import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { Hit, HitSchema } from '../shared/schemas/hit.schema';
import { TasksController } from './controllers/tasks.controller';
import { HitsRepository } from './repositories/hits.repository';
import { TasksService } from './services/tasks.service';
import { TimeService } from './services/time.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    HttpModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        baseURL: configService.get('API_BASE_URL'),
      }),
    }),
    MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }]),
  ],
  providers: [TasksService, TimeService, HitsRepository],
  controllers: [TasksController],
})
export class TasksModule {}
