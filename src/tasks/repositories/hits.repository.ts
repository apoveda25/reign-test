import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { IHit } from '../../shared/interfaces/payload.interface';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';

@Injectable()
export class HitsRepository {
  constructor(@InjectModel(Hit.name) private hitModel: Model<HitDocument>) {}

  async create(createHitDto: IHit): Promise<HitDocument> {
    return await this.hitModel.create({
      ...createHitDto,
      _id: new Types.ObjectId(),
    });
  }

  async findOne(args: {
    _id?: string;
    objectID?: string;
  }): Promise<HitDocument> {
    return await this.hitModel.findOne(args).exec();
  }
}
