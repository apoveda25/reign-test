import faker from '@faker-js/faker';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { IHit } from '../../../dist/tasks/interfaces/payload.interface';
import { Hit, HitDocument } from '../../shared/schemas/hit.schema';
import { HitsRepository } from './hits.repository';

describe('HitsRepository', () => {
  let provider: HitsRepository;
  let hitModel: Model<HitDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HitsRepository,
        {
          provide: getModelToken(Hit.name),
          useValue: { findOne: jest.fn(), exec: jest.fn(), create: jest.fn() },
        },
      ],
    }).compile();

    provider = module.get<HitsRepository>(HitsRepository);
    hitModel = module.get<Model<HitDocument>>(getModelToken(Hit.name));
  });

  test('should be defined', () => {
    expect(provider).toBeDefined();
    expect(hitModel).toBeDefined();
  });

  describe('findOne', () => {
    test('should return one hit', async () => {
      /**
       * Arrange
       */
      const args: {
        _id?: string;
        objectID?: string;
      } = {
        _id: faker.datatype.uuid(),
        objectID: faker.datatype.uuid(),
      };

      const hitModelFindOneSpy = jest
        .spyOn(hitModel, 'findOne')
        .mockImplementation()
        .mockReturnThis();

      /**
       * Act
       */
      await provider.findOne(args);

      /**
       * Assert
       */
      expect(hitModelFindOneSpy).toHaveBeenCalledWith(args);
    });
  });

  describe('create', () => {
    test('should return one article created', async () => {
      /**
       * Arrange
       */
      const createHitDto: IHit = {
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };

      const hitModelCreateSpy = jest
        .spyOn(hitModel, 'create')
        .mockImplementation();

      /**
       * Act
       */
      await provider.create(createHitDto);

      /**
       * Assert
       */
      expect(hitModelCreateSpy).toHaveBeenCalled();
    });
  });
});
