import faker from '@faker-js/faker';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { IHit } from 'src/shared/interfaces/payload.interface';
import { TasksService } from '../services/tasks.service';
import { TimeService } from '../services/time.service';
import { TasksController } from './tasks.controller';

describe('TasksController', () => {
  let controller: TasksController;
  let tasksService: TasksService;
  let httpService: HttpService;
  let timeService: TimeService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [
        {
          provide: TasksService,
          useFactory: () => ({
            getHitsIterator: jest.fn(),
            saveHitsFromIterator: jest.fn(),
          }),
        },
        {
          provide: HttpService,
          useFactory: () => ({}),
        },
        {
          provide: TimeService,
          useFactory: () => ({}),
        },
        {
          provide: ConfigService,
          useFactory: () => ({ get: jest.fn() }),
        },
      ],
    }).compile();

    controller = module.get<TasksController>(TasksController);
    tasksService = module.get<TasksService>(TasksService);
    httpService = module.get<HttpService>(HttpService);
    timeService = module.get<TimeService>(TimeService);
    configService = module.get<ConfigService>(ConfigService);
  });

  test('should be defined', () => {
    expect(controller).toBeDefined();
    expect(tasksService).toBeDefined();
    expect(httpService).toBeDefined();
    expect(timeService).toBeDefined();
    expect(configService).toBeDefined();
  });

  describe('handleCron', () => {
    test('should return void', async () => {
      /**
       * Arrange
       */
      const article: IHit = {
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };
      const asyncIterator = async function* () {
        yield [article];
      };
      const iterator = asyncIterator();
      const tasksServiceGetHitsIteratorSpy = jest
        .spyOn(tasksService, 'getHitsIterator')
        .mockReturnValue(iterator);

      const tasksServiceSaveHitsFromIteratorSpy = jest.spyOn(
        tasksService,
        'saveHitsFromIterator',
      );

      /**
       * Act
       */
      const result = await controller.handleCron();

      /**
       * Assert
       */
      expect(tasksServiceGetHitsIteratorSpy).toHaveBeenCalledWith({
        httpService,
        timeService,
        initPage: 0,
      });
      expect(tasksServiceSaveHitsFromIteratorSpy).toHaveBeenCalledWith(
        iterator,
      );
      expect(result).toEqual(undefined);
    });
  });

  describe('handleTimeout', () => {
    test('should return void', async () => {
      /**
       * Arrange
       */
      const SCHEDULE_SAVE_ALL_KEY = 'SCHEDULE_SAVE_ALL';
      const SCHEDULE_SAVE_ALL_VALUE = 'TRUE';
      const article: IHit = {
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };
      const asyncIterator = async function* () {
        yield [article];
      };
      const iterator = asyncIterator();
      const configServiceGetSpy = jest
        .spyOn(configService, 'get')
        .mockReturnValue(SCHEDULE_SAVE_ALL_VALUE);

      const tasksServiceGetHitsIteratorSpy = jest
        .spyOn(tasksService, 'getHitsIterator')
        .mockReturnValue(iterator);

      const tasksServiceSaveHitsFromIteratorSpy = jest.spyOn(
        tasksService,
        'saveHitsFromIterator',
      );

      /**
       * Act
       */
      const result = await controller.handleTimeout();

      /**
       * Assert
       */
      expect(configServiceGetSpy).toHaveBeenCalledWith(SCHEDULE_SAVE_ALL_KEY);
      expect(tasksServiceGetHitsIteratorSpy).toHaveBeenCalledWith({
        httpService,
        timeService,
        initPage: 0,
        getAllHits: true,
      });
      expect(tasksServiceSaveHitsFromIteratorSpy).toHaveBeenCalledWith(
        iterator,
      );
      expect(result).toEqual(undefined);
    });
  });
});
