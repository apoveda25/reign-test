import { HttpService } from '@nestjs/axios';
import { Controller, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, Timeout } from '@nestjs/schedule';
import { TasksService } from '../services/tasks.service';
import { TimeService } from '../services/time.service';

@Controller()
export class TasksController {
  private readonly logger = new Logger(TasksController.name);

  constructor(
    private tasksService: TasksService,
    private httpService: HttpService,
    private timeService: TimeService,
    private configService: ConfigService,
  ) {}

  @Cron('0 0 */1 * * *')
  async handleCron(): Promise<void> {
    this.logger.debug(`Running cron to save hits from the last hour`);

    const hitsIterator = this.tasksService.getHitsIterator({
      httpService: this.httpService,
      timeService: this.timeService,
      initPage: 0,
    });

    await this.tasksService.saveHitsFromIterator(hitsIterator);

    this.logger.debug(`Cron terminated to save hits from the last hour`);
  }

  @Timeout(100)
  async handleTimeout(): Promise<void> {
    if (this.configService.get<string>('SCHEDULE_SAVE_ALL') === 'TRUE') {
      this.logger.debug(`Running timeout to save all hits`);

      const hitsIterator = this.tasksService.getHitsIterator({
        httpService: this.httpService,
        timeService: this.timeService,
        initPage: 0,
        getAllHits: true,
      });

      await this.tasksService.saveHitsFromIterator(hitsIterator);

      this.logger.debug(`Timeout over to save all hits`);
    }
  }
}
