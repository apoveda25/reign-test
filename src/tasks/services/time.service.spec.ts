import { Test, TestingModule } from '@nestjs/testing';
import { TimeService } from './time.service';

describe('TimeService', () => {
  let service: TimeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TimeService],
    }).compile();

    service = module.get<TimeService>(TimeService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getHourExactMin', () => {
    test('should return one date', async () => {
      /**
       * Arrange
       */
      const date = new Date('2022-04-02T23:04:46.054Z');
      const dateExacMin = new Date('2022-04-02T23:00:00.000Z');

      /**
       * Act
       */
      const result = service.getHourExactMin(date);

      /**
       * Assert
       */
      expect(result).toEqual(dateExacMin);
    });
  });

  describe('getHourExactMax', () => {
    test('should return one date', async () => {
      /**
       * Arrange
       */
      const date = new Date('2022-04-02T23:04:46.054Z');
      const dateExacMax = new Date('2022-04-02T23:59:59.999Z');

      /**
       * Act
       */
      const result = service.getHourExactMax(date);

      /**
       * Assert
       */
      expect(result).toEqual(dateExacMax);
    });
  });

  describe('getAndSetHour', () => {
    test('should return one date with -3 hours', async () => {
      /**
       * Arrange
       */
      const date = new Date('2022-04-02T23:04:46.054Z');
      const hourDelta = -3;
      const dateSet = new Date('2022-04-02T20:04:46.054Z');

      /**
       * Act
       */
      const result = service.getAndSetHour(date, hourDelta);

      /**
       * Assert
       */
      expect(result).toEqual(dateSet);
    });

    test('should return one date with +3 hours', async () => {
      /**
       * Arrange
       */
      const date = new Date('2022-04-02T23:04:46.054Z');
      const hourDelta = 3;
      const dateSet = new Date('2022-04-03T02:04:46.054Z');

      /**
       * Act
       */
      const result = service.getAndSetHour(date, hourDelta);

      /**
       * Assert
       */
      expect(result).toEqual(dateSet);
    });
  });
});
