import { Injectable } from '@nestjs/common';

@Injectable()
export class TimeService {
  getHourExactMin(date: Date): Date {
    const dateExacMin = new Date(date.toISOString());

    dateExacMin.setMinutes(0, 0, 0);

    return dateExacMin;
  }

  getHourExactMax(date: Date): Date {
    const dateExacMax = new Date(date.toISOString());

    dateExacMax.setMinutes(59, 59, 999);

    return dateExacMax;
  }

  getAndSetHour(date: Date, hourDelta: number): Date {
    const dateSet = new Date(date.toISOString());

    dateSet.setHours(dateSet.getHours() + hourDelta);

    return dateSet;
  }
}
