import faker from '@faker-js/faker';
import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { AxiosResponse } from 'axios';
import mongoose from 'mongoose';
import { Observable } from 'rxjs';
import { IHit, IPayload } from '../../shared/interfaces/payload.interface';
import { HitDocument } from '../../shared/schemas/hit.schema';
import { HitsRepository } from '../repositories/hits.repository';
import { TasksService } from './tasks.service';
import { TimeService } from './time.service';

describe('TasksService', () => {
  let service: TasksService;
  let hitsRepository: HitsRepository;
  let httpService: HttpService;
  let timeService: TimeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TasksService,
        {
          provide: HitsRepository,
          useFactory: () => ({ findOne: jest.fn(), create: jest.fn() }),
        },
        {
          provide: HttpService,
          useFactory: () => ({ get: jest.fn() }),
        },
        {
          provide: TimeService,
          useFactory: () => ({
            getHourExactMin: jest.fn(),
            getAndSetHour: jest.fn(),
          }),
        },
      ],
    }).compile();

    service = module.get<TasksService>(TasksService);
    hitsRepository = module.get<HitsRepository>(HitsRepository);
    httpService = module.get<HttpService>(HttpService);
    timeService = module.get<TimeService>(TimeService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
    expect(hitsRepository).toBeDefined();
    expect(httpService).toBeDefined();
    expect(timeService).toBeDefined();
  });

  describe('getHitsIterator', () => {
    test('should return one async generator array of hits', async () => {
      /**
       * Arrange
       */
      const article: IHit = {
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };
      const args = {
        httpService,
        timeService,
        initPage: 0,
        getAllHits: false,
      };
      const dateMax = new Date('2022-04-02T23:00:00.000Z');
      const dateMin = new Date('2022-04-02T22:00:00.000Z');
      const response$ = new Observable((subscriber) => {
        subscriber.next({
          data: {
            page: 0,
            nbPages: 2,
            hits: [article],
            nbHits: 10,
            hitsPerPage: 5,
            exhaustiveNbHits: true,
            exhaustiveTypo: false,
            query: '',
            params: '',
            processingTimeMS: 0,
          },
          status: 200,
          statusText: '',
          headers: {},
          config: {},
        });
      }) as Observable<AxiosResponse<IPayload, any>>;

      const timeServiceGetHourExactMinSpy = jest
        .spyOn(timeService, 'getHourExactMin')
        .mockReturnValue(dateMax);

      const timeServiceGetAndSetHourSpy = jest
        .spyOn(timeService, 'getAndSetHour')
        .mockReturnValue(dateMin);

      const httpServiceGetSpy = jest
        .spyOn(httpService, 'get')
        .mockReturnValue(response$);

      /**
       * Act
       */
      const result = service.getHitsIterator(args).next();

      /**
       * Assert
       */
      expect(timeServiceGetHourExactMinSpy).toHaveBeenCalled();
      expect(timeServiceGetAndSetHourSpy).toHaveBeenCalled();
      expect(httpServiceGetSpy).toHaveBeenCalled();
      expect(result).toBeInstanceOf(Promise);
    });
  });

  describe('saveHitsFromIterator', () => {
    test('should return one promise void', async () => {
      /**
       * Arrange
       */
      const article: IHit = {
        created_at: faker.date.past(),
        title: faker.name.title(),
        url: faker.internet.url(),
        author: faker.name.firstName(),
        points: faker.datatype.number(),
        story_text: faker.lorem.paragraph(),
        comment_text: faker.lorem.paragraph(),
        num_comments: faker.datatype.number(),
        story_id: faker.datatype.uuid(),
        story_title: faker.name.title(),
        story_url: faker.internet.url(),
        parent_id: null,
        created_at_i: faker.date.past().getTime(),
        _tags: [faker.name.jobType()],
        objectID: faker.datatype.uuid(),
        _highlightResult: {
          title: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          author: {
            value: faker.name.firstName(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          story_text: {
            value: faker.name.title(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
          url: {
            value: faker.internet.url(),
            matchLevel: faker.name.prefix(),
            fullyHighlighted: faker.datatype.boolean(),
            matchedWords: [faker.lorem.word()],
          },
        },
      };
      const articleCreated = {
        ...article,
        deleted: false,
        _id: new mongoose.Types.ObjectId(),
      } as HitDocument;
      const asyncIterator = async function* () {
        yield [article];
      };
      const findOneDto = {
        objectID: article.objectID,
      };

      const hitsRepositoryFindOneSpy = jest
        .spyOn(hitsRepository, 'findOne')
        .mockResolvedValue(null);

      const hitsRepositoryCreateSpy = jest
        .spyOn(hitsRepository, 'create')
        .mockResolvedValue(articleCreated);

      /**
       * Act
       */
      const result = await service.saveHitsFromIterator(asyncIterator());

      /**
       * Assert
       */
      expect(hitsRepositoryFindOneSpy).toHaveBeenCalledWith(findOneDto);
      expect(hitsRepositoryCreateSpy).toHaveBeenCalledWith(article);
      expect(result).toEqual(undefined);
    });
  });
});
