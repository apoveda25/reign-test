import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { lastValueFrom } from 'rxjs';
import { IHit, IPayload } from '../../shared/interfaces/payload.interface';
import { HitsRepository } from '../repositories/hits.repository';
import { TimeService } from './time.service';

@Injectable()
export class TasksService {
  constructor(private hitsRepository: HitsRepository) {}

  getHitsIterator = async function* ({
    httpService,
    timeService,
    initPage = 0,
    getAllHits = false,
  }: {
    httpService: HttpService;
    timeService: TimeService;
    initPage: number;
    getAllHits?: boolean;
  }) {
    const dateMax = timeService.getHourExactMin(new Date());
    const dateMin = timeService.getAndSetHour(dateMax, -1);
    let currentPage = new Number(initPage);
    let lastPage = initPage + 1;

    while (currentPage <= lastPage) {
      try {
        const path = getAllHits
          ? `search_by_date?query=nodejs&tags=story&page=${currentPage}`
          : `search_by_date?query=nodejs&tags=story&page=${currentPage}&numericFilters=created_at_i>${dateMin.getTime()},created_at_i<${dateMax.getTime()}`;

        const { data } = await lastValueFrom(httpService.get<IPayload>(path));

        currentPage = data.page + 1;
        lastPage = data.nbPages;

        yield data.hits;
      } catch (error) {
        currentPage = lastPage + 1;
      }
    }
  };

  async saveHitsFromIterator(
    hitsIterator: AsyncGenerator<IHit[], void, unknown>,
  ): Promise<void> {
    for await (const iterator of hitsIterator) {
      for (const hit of iterator) {
        const hitExist = await this.hitsRepository.findOne({
          objectID: hit.objectID,
        });

        if (!hitExist) await this.hitsRepository.create(hit);
      }
    }
  }
}
